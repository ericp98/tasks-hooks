import React, { Fragment } from "react";
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Tasks from './Task'

function App(): JSX.Element {
  return (
    <Fragment>
      <Router>
        <Route path='/' component={Tasks}/>
      </Router>
      
    </Fragment>
  );
}

export default App;
