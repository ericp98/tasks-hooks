import React, { Fragment, useState, useRef } from "react";

// Tipos
type FormElement = React.FormEvent<HTMLFormElement>;

// Interfaz de tarea
interface ITask {
  name: string;
  done: boolean;
}

function Tasks(): JSX.Element {
  const [newTask, setNewTask] = useState<string>("");
  const [tasks, setTasks] = useState<ITask[]>([]);
  const taskInput = useRef<HTMLInputElement>(null);

  const handleSubmit = (e: FormElement) => {
    e.preventDefault();
    addTask(newTask);
    setNewTask("");
    taskInput.current?.focus();
  };

  const addTask = (name: string): void => {
    const newTasks: ITask[] = [...tasks, { name, done: false }];
    setTasks(newTasks);
  };

  const toggleDonetask = (i: number) : void => {
    const newTasks: ITask[] = [...tasks];
    newTasks[i].done = !newTasks[i].done;
    setTasks(newTasks);
  }

  const removeTask = (i: number) : void => {
    const newTasks: ITask[] = [...tasks];
    newTasks.splice(i,1);
    setTasks(newTasks);
  }

  return (
    <Fragment>
      <div className="container p-4">
        <div className="row">
          <div className="col-md-6 offset-md-3">
            <div className="card mb-5">
              <div className="card-body" style={{boxShadow: '10px 10px 10px 10px #34388A'}}>
                <form onSubmit={(e) => handleSubmit(e)}>
                  <input
                    type="text"
                    value={newTask}
                    onChange={(e) => setNewTask(e.target.value)}
                    className="form-control"
                    ref={taskInput}
                    autoFocus
                    placeholder="Ingrese una tarea"
                  />
                  <button className="btn btn-success btn-block mt-2">Guardar</button>
                </form>
              </div>
            </div>

            {tasks.map((task: ITask, indice: number) => {
              return (
                <div className="card card-body mt-2" style={{boxShadow: '10px 10px 10px 10px #34388A'}} key={indice}>
                  <h2 style={{textDecoration: task.done ? 'line-through' : ''}}>{task.name}</h2>
                  <div>
                    <button className="btn btn-secondary mr-2" onClick={() => toggleDonetask(indice)}>
                      {task.done ? '✓' : '✗'}
                    </button>
                    <button className="btn btn-danger" onClick={() => removeTask(indice)}>
                    🗑
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default Tasks;